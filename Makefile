CC=gcc

CFLAGS=-c -Wall

all: program
	
program: main.o klient.o
	$(CC) -Wall -o program main.o klient.o 
	
main.o: main.c moduly.h
	$(CC) $(CFLAGS) main.c
	
klient.o: klient.c
	$(CC) $(CFLAGS) klient.c
	
clean:
	rm -rf *o program
