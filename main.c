//
//  main.c
//  9
//
/*Stwórz program w pliku .c, w którym będzie main() i który będzie miał załączony plik moduly.h. Program ma być w stanie obsłużyć:
• dodanie klienta (1 lub więcej naraz, użytkownik wskazuje ile i podaje nazwiska) • usunięcie klienta o danym nazwisku (uzupełnić pop by surname() w module)
• wyświetlenie ilości klientów
• wyświetlenie nazwisk klientów
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "moduly.h"

int main()
{
    struct Client *head;
    head = (struct Client*)malloc(sizeof(struct Client));
    head = NULL;
    
    int option = 1;
    
    while (option > 0)
    {
        printf("\nm e n u\n1. add a client\n2. remove a client by given surname\n3. display the number of clients\n4. display all customers’ surnames\n5. exit\n");
        scanf("%d", &option);
    
        switch (option)
        {
            case 1:
            {
                int x = 0;
                printf("\nhow many clients do you want to add to the list? (1 or more)\n");
                scanf("%d", &x);
                while(x > 0)
                {
                    char tmp[50] = "";
                    char surname[50] = "";
                    printf("enter the surname: ");
                    scanf("%50s", tmp);
                    
                    strcpy(surname, tmp);
                    
                    push_back(&head, surname);
                    
                    x--;
                }
            }
                break;
            case 2:
            {
                char surname2[50] = "";
                printf("\nenter the surname of the client you want to delete:\n");
                scanf("%50s", surname2);
                pop_by_surname(&head, surname2);
            }
                break;
                
            case 3:
                printf("\nnumber of clients: %d\n", list_size(head));
                break;
            case 4:
                show_list(head);
                break;
            case 5:
                exit(0);
                break;
            default:
                printf("\nyou entered an invalid choice!\nplease try again\n");
                break;
        }

    }
     
    printf("\n");
    return 0;
}


